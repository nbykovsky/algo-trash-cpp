/*
 *Implementation of median 
 */

#ifndef median_h
#define median_h

#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <functional>
#include "pqueue.h"


template <class T>
class median {
    private:
        pqueue<T> minheap;
        pqueue<T> maxheap;
        function<bool(T,T)> cmp;
    public:
        median(function<bool(T,T)> cmp);
        void add(T elem);
        void del(T elem);
        T value();
        long int size();
};


template<class T>
median<T>::median(function<bool(T,T)> cmp){
    this->cmp = cmp;
    minheap = pqueue<T>([cmp](T a, T b){return !cmp(a, b);});
    maxheap = pqueue<T>(cmp);
}

template<class T>
long int median<T>::size(){
    return minheap.size()+maxheap.size();
}

template<class T>
void median<T>::add(T elem){
    if(minheap.isempty()&&maxheap.isempty()) {
        minheap.push(elem);
    }else if((minheap.isempty()&&cmp(maxheap.top(),elem)) || !cmp(elem, minheap.top())) {
        minheap.push(elem);
        while(minheap.size()>maxheap.size()+1){
            maxheap.push(minheap.top());
            minheap.pop();
        }
    }else {
        maxheap.push(elem);
        while(maxheap.size()>minheap.size()+1){
            minheap.push(maxheap.top());
            maxheap.pop();
        }
    }
}

template<class T>
void median<T>::del(T elem){
    if(!minheap.isempty()&&!cmp(elem,minheap.top())){
        minheap.del(elem);
        while(maxheap.size()>minheap.size()+1){
            minheap.push(maxheap.top());
            maxheap.pop();
        }
    }else{
        maxheap.del(elem);
        while(minheap.size()>maxheap.size()+1){
            maxheap.push(minheap.top());
            minheap.pop();
        }
    }
}

template<class T>
T median<T>::value(){
    if(maxheap.size()>minheap.size()) return maxheap.top();
    else if (minheap.size()>maxheap.size()) return minheap.top();
    else return (maxheap.top()+minheap.top())/2;

}



#endif
