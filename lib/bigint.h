/*
 * basic operations with very big numbers
 */
#ifndef bigint_h
#define bigint_h

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>

using namespace std;

typedef long long ull;

class bigint {
    private:
        vector<ull> payload;
        const ull base = 9;
        const ull base10 = (ull)pow(10, base);
    public:
        bigint() = default;

        bigint(const string &str) {
            payload = vector<ull>(str.size());
            transform(str.begin(), str.end(), payload.begin(), [](const char a){return a - '0';});
            reverse(payload.begin(), payload.end());
        }

        bigint(const vector<ull> v){
            payload = v;
        }

        void add(bigint &that){
            ull s1 = payload.size();
            ull s2 = that.payload.size();
            vector<ull> result(max(s1, s2));
            ull next=0;
            for(ull i=0;i<result.size();i++){
                if(i<s1) next+=payload[i];
                if(i<s2) next+=that.payload[i];
                result[i]=next%base10;
                next/=base10;
            }
            if(next) result.push_back(next);
            payload = result;
        }

        void mult(bigint that){
            ull s1 = payload.size();
            ull s2 = that.payload.size();
            vector<ull> result(s1+s2,0);
            ull i1, i2;
            for(i1=0;i1<s1;i1++){
                ull next=0;
                ull pi1 = payload[i1];
                for(i2=0;i2<s2;i2++){
                    ull i12 = i1+i2;
                    next+=pi1*that.payload[i2] + result[i12];
                    result[i12]=next%base10;
                    next/=base10;
                }
                if(next) result[i1+i2]+=next;
            }
            ull j=result.size()-1;
            while(result[j]==0&&j--) result.pop_back();
            payload=result;
        }

        string to_str(){
            string str;
            vector<ull> tmp(payload);
            reverse(tmp.begin(), tmp.end());
            for(int i = 0; i < tmp.size(); ++i){
                int nd = tmp[i] > 0 ? (int) log10 ((double) tmp[i]) + 1 : 1;
                if (nd < base && i != 0){
                    for (int j = 0; j < base - nd; ++j)
                        str+="0";
                }
                str+=to_string(tmp[i]);
            }
            return str;
        }
};

#endif
