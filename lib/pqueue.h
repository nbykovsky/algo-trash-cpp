#ifndef pqueue_h
#define pqueue_h

#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <functional>

using namespace std;

template <class T>
class pqueue{
    private:
        vector<T> elms;
        map<T, set<long int>> idx;
        function<bool(T, T)> cmp;
        void up(long int id);
        void down(long int id);
        long int left(long int id);
        long int right(long int id);
        long int parent(long int id);
    public:
        pqueue(){}
        pqueue(vector<T> const &vct, function<bool(T, T)> cmp);
        pqueue(function<bool(T,T)> cmp);
        void push(T const &v);
        void pop();
        T top();
        void del(T const &v);
        bool isempty();
        void show();
        long int size();
};

template <class T>
pqueue<T>::pqueue(vector<T> const &vct, function<bool(T, T)> cmp){
    this->cmp = cmp;
    for(const auto &v: vct) push(v);
}

template <class T>
long int pqueue<T>::size(){
    return elms.size();
}

template <class T>
pqueue<T>::pqueue(function<bool(T, T)> cmp){
    this->cmp = cmp;
}

template <class T>
bool pqueue<T>::isempty(){
    return (elms.size()==0);
}

template <class T>
void pqueue<T>::push(T const &elem){
    elms.push_back(elem);
    if(!idx.count(elem)) idx[elem] = set<long int>();
    idx[elem].insert(elms.size()-1);
    up(elms.size()-1);
}

template <class T>
void pqueue<T>::pop(){
    if(elms.size() == 0) return;
    long int last = elms.size()-1;
    idx[elms[0]].erase(0);
    idx[elms[last]].erase(last);
    idx[elms[last]].insert(0);
    elms[0] = elms[last];
    elms.pop_back();
    down(0);
}

template <class T>
T pqueue<T>::top(){
    return elms.front();
}

template <class T>
void pqueue<T>::del(T const &v){
    if(!idx.count(v)) return;
    if(v==elms[0]) {
        pop();
        return;
    }
    long int last = elms.size()-1;
    long int dlt = (*idx[v].begin());
    idx[v].erase(dlt);
    idx[elms[last]].erase(last);
    idx[elms[last]].insert(dlt);
    elms[dlt] = elms[last];
    elms.pop_back();
    if(cmp(elms[parent(dlt)], elms[dlt])) up(dlt);
    else down(dlt);
}

template <class T>
long int pqueue<T>::left(long int id){
    long int l = id * 2 + 1;
    if(l < elms.size())
        return l;
    else
        return -1;
}

template <class T>
long int pqueue<T>::right(long int id){
    long int r = id * 2 + 2;
    if(r < elms.size())
        return r;
    else
        return -1;
}

template <class T>
long int pqueue<T>::parent(long int id){
    if(id==0)
        return -1;
    else
        return (id - 1)/2;
}

template <class T>
void pqueue<T>::up(long int id){
    if(id >=0 && parent(id) >=0 && cmp(elms[parent(id)], elms[id])){
        long int tmp = elms[id];
        idx[elms[id]].erase(id);
        idx[elms[parent(id)]].insert(id);
        idx[elms[parent(id)]].erase(parent(id));
        idx[elms[id]].insert(parent(id));
        elms[id] = elms[parent(id)];
        elms[parent(id)] = tmp;
        up(parent(id));
    }
}

template <class T>
void pqueue<T>::down(long int id){
    long int c = left(id);
    long int c1 = right(id);
    if(c >= 0 && c1 >= 0 && cmp(elms[c], elms[c1])) c = c1;
    if(c > 0 && cmp(elms[id], elms[c])){
        long int tmp = elms[id];
        idx[elms[id]].erase(id);
        idx[elms[c]].insert(id);
        idx[elms[c]].erase(c);
        idx[elms[id]].insert(c);
        elms[id] = elms[c];
        elms[c] = tmp;
        down(c);
    }

}

template <class T>
void pqueue<T>::show(){
    for(const auto &e: elms) cout << e << " ";
    cout << endl;
}



#endif
