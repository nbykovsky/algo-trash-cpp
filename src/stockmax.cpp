/*
 * problem https://www.hackerrank.com/challenges/stockmax/problem
 */

#include <iostream>
#include <vector>

using namespace std;

typedef long long int ll;

int main(){
    int t;
    cin >> t;
    vector<ll> ans(t);
    for(int i=0;i<t;i++){
        int n;
        cin >> n;
        vector<int> prises(n);
        for(int j=0;j<n;j++) cin >> prises[j];
        vector<int> max_right_prise(n,0);
        max_right_prise[n-1] = prises[n-1];
        for(int j=n-2;j>=0; j--) max_right_prise[j] = max(max_right_prise[j+1], prises[j]);
        ll position=0;
        ll profit=0;
        ll fixed_profit=0;
        for(int j=0;j<n;j++){
            if(position>0) profit+=(prises[j] - prises[j-1]) * position;
            if(max_right_prise[j]==prises[j]){
                fixed_profit+=profit;
                profit=0;
                position=0;
            } else if(max_right_prise[j]>prises[j]) position++;
        }
        ans[i] = fixed_profit;
    }
    for(const auto &a: ans) cout << a << endl;

    return 0;
}