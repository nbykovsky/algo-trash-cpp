// problem https://www.hackerrank.com/challenges/alternating-characters/problem
#include <iostream>
#include <vector>
using namespace std;

int main() {
    int q;
    cin >> q;
    cin.ignore();
    vector<int> ans(q,0);
    for(int i=0;i<q;i++){
        string s;
        getline(cin, s);

        int counter=0;
        for(int j=1;j<s.length();j++){
            if(s[j]!=s[j-1]){
                ans[i]+=counter;
                counter=0;
            } else {
                counter++;
            }
        }
        ans[i]+=counter;
    }

    for(auto const &a: ans) cout << a << endl;
}