// https://www.hackerrank.com/challenges/largest-rectangle/problem
#include <vector>
#include <stack>
#include <iostream>
#include <utility>

using namespace std;

typedef long long int lli;

int main(){
    int n;
    cin >> n;
    vector<lli> h(n+1, 0);
    for(auto i=0;i<n;i++) cin >> h[i];

    stack<pair<lli, lli>> st;
    lli ans = n;
    for(auto i=0;i<n+1;i++){
        lli leftInd = i;
        while(!st.empty() and st.top().second >= h[i]){
            leftInd = st.top().first;
            ans = max(ans, h[i] * (i - leftInd + 1));
            ans = max(ans,  st.top().second * (i - leftInd));
            st.pop();
        }
        st.push(make_pair(leftInd, h[i]));
    }
    cout << ans << endl;
}