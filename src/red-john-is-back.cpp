/*
 * problem https://www.hackerrank.com/challenges/red-john-is-back/problem
 */

#include <iostream>
#include <vector>

using namespace std;

typedef long long int ll;

ll eratospher(ll num){
    vector<bool> er(num+1, true);
    ll counter = 0;
    for(ll i=2; i<=num;i++){
        if(er[i]){
            counter++;
            ll a = i;
            while(a <= num){
                er[a] = false;
                a+=i;
            };
        }
    }
    return counter;
}

int main(){
    int t;
    cin >> t;
    vector<ll> ans(t);
    for(int i=0;i<t;i++){
        int n;
        cin >> n;
        vector<ll> dp(n+1, 0);
        vector<int> op = {1, 4};
        dp[0] = 1;
        for(int j=1;j<=n;j++)
            for(const auto &o: op) if(o<=j) dp[j]+=dp[j-o];

        ans[i] = eratospher(dp[n]);
    }
    for(const auto &a: ans) cout << a << endl;
    return 0;
}