//problem https://www.hackerrank.com/challenges/crush/problem
#include<iostream>
#include<vector>
#include<utility>
#include<algorithm>

using namespace std;

int main(){
    int n,m;
    cin >> n >> m;
    vector<pair<int,int>> queries;
    for(int i=0;i<m;i++){
        pair<int,int> p;
        int a, b, k;
        cin >> a >> b >> k;
        queries.emplace_back(a, k);
        queries.emplace_back(b + 1, -k);
    }

    sort(queries.begin(),queries.end());

    long mx = 0;
    long acc = 0;
    for(auto const &q: queries) {
        acc+=q.second;
        if (acc > mx) mx = acc;
    }
    cout << mx << endl;
}