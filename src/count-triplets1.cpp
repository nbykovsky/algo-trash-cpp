//problem https://www.hackerrank.com/challenges/count-triplets-1/problem
#include <iostream>
#include <map>
#include <vector>

using namespace std;

int main(){

    unsigned long n;
    long r;
    cin >> n >> r;
    vector<long> numArray(n);
    for(long i=n-1;i>=0;i--) cin >> numArray[i];

    long counter=0;
    map<long, long> m1;
    if(r==1){
        for(long &a: numArray) {
            if (m1.count(a) and m1[a] > 1) {
                counter += m1[a] * (m1[a] - 1) / 2;
            }
            if(m1.count(a)) m1[a]++;
            else m1[a]=1;
        }

    }else{
        map<long, long> m2;

        for(long &a: numArray){

            if(m1.count(a)) m1[a]++;
            else m1[a]=1;

            if(m1.count(a*r)) {
                if(m2.count(a)) m2[a]+=m1[a*r];
                else m2[a] = m1[a*r];
            }

            if(m2.count(a*r)) counter+=m2[a*r];

        }

    }

    cout << counter << endl;
}

