//
// Created by nikolai on 3/10/18.
//

#include <iostream>
#include <vector>

using namespace std;


int main(){
    int n;
    cin >> n;
    for(int i=0;i<n;i++){
        unsigned int t;
        cin >> t;
        vector<int> q(t);
        for(int j=0;j<t;j++) cin >> q[j];

        int counter = 0;
        bool good = true;

        int run = 1;
        for(int j=t-1;j>0;j--){

            int k = j;
            while(q[k]<q[k-1] && k > 0){
                int tmp = q[k];
                q[k] = q[k-1];
                q[k-1] = tmp;
                k--;
                counter++;
            }
            if (run == 1 && j == 1){
                run++;
                j = t;
            }

        }
        for(int j=1;j<t;j++){
            if(q[j]<q[j-1]) {
                good=false;
                break;
            }
        }

        if (not good) cout << "Too chaotic" << endl;
        else cout << counter << endl;

    }

}