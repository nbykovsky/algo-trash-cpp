/*
 * problem https://www.hackerrank.com/challenges/lego-blocks/problem
 */

#include <iostream>
#include <vector>

using namespace std;

typedef long long int ull;

int main(){
    const vector<int> ls = {1,2,3,4};
    const ull base = 1000000007;
    int t;
    cin >> t;
    vector<ull> ans(t);
    for(int i=0;i<t;i++){
        int n,m;
        cin >> n >> m;
        vector<ull> dp(m+1, 0);
        vector<ull> walls(m+1, 0);
        vector<ull> without_walls(m+1, 0);
        dp[0] = 1;
        walls[0] = 1;
        without_walls[0] = 1;
        for(int j=1; j<=m; j++) {
            for (const int &l: ls) {
                if (l <= j) {
                    dp[j] = (dp[j]%base + dp[j - l]%base);
                }
            }
        }
        for(int j=1; j<=m; j++) {
            ull e = dp[j];
            for(int x=1; x<n; x++)  dp[j] = ((dp[j]%base)*e);
        }

        for(int j=1; j<=m; j++) {
            for(int k=1;k<j;k++){
                walls[j]=(walls[j] + ((without_walls[k])*(dp[j-k]%base)))%base;
            }
            without_walls[j] = (dp[j] - walls[j])%base;
            if (without_walls[j] < 0) without_walls[j]+=base;
        }
        ans[i] = without_walls[m]%base;
    }
    for(const auto &a: ans) cout << a << endl;

    return 0;
}
