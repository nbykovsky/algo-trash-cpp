/*
 * problem https://www.hackerrank.com/challenges/knightl-on-chessboard/problem
 */

#include <iostream>
#include <queue>
#include <map>
#include <vector>

using namespace std;

vector<pair<int,int>> getmoves(pair<int, int> point, int n, pair<int, int> move){
    vector<pair<int,int>> tmp;
    tmp.push_back(make_pair(point.first - move.first, point.second - move.second));
    tmp.push_back(make_pair(point.first - move.first, point.second + move.second));
    tmp.push_back(make_pair(point.first + move.first, point.second - move.second));
    tmp.push_back(make_pair(point.first + move.first, point.second + move.second));
    tmp.push_back(make_pair(point.first - move.second, point.second - move.first));
    tmp.push_back(make_pair(point.first + move.second, point.second - move.first));
    tmp.push_back(make_pair(point.first - move.second, point.second + move.first));
    tmp.push_back(make_pair(point.first + move.second, point.second + move.first));
    vector<pair<int,int>> output;
    for(const auto &t: tmp) if(t.first>=0&&t.first<n&&t.second>=0&&t.second<n) output.push_back(t);
    return output;
}

int bfs(pair<int,int> start, pair<int, int> finish, int n, pair<int, int> move){
    queue<pair<int,int>> q;
    map<pair<int,int>, int> dist;
    bool stop = false;
    dist[start]=0;
    q.push(start);
    while(!q.empty()&&!stop){
        pair<int,int> from = q.front();
        q.pop();
        for(const auto &to: getmoves(from, n, move)){
            if(dist.count(to)) continue;
            dist[to] = dist[from] + 1;
            q.push(to);
            if(to==finish){
                stop=true;
                break;
            }
        }
    }
    if(!stop) return -1;
    else return dist[finish];
}

int main(){
    int n;
    cin >> n;
    for(int r=1;r<n;r++){
        for(int c=1;c<n;c++){
            cout << bfs(make_pair(0,0), make_pair(n-1,n-1),n,make_pair(r,c)) << " ";
        }
        cout << endl;
    }
    return 0;
}
