// https://www.hackerrank.com/challenges/minimum-time-required/problem
#include <iostream>
#include <vector>
#include <cmath>
#include <climits>

using namespace std;

typedef unsigned long long int lli;


lli count(vector<lli> &m, lli days){
    lli cnt=0;
    for(auto const &mm: m){
        cnt+=days/mm;
    }
    return cnt;
}

lli search(vector<lli> &m, lli goal, lli b, lli e){
    if(b + 1 == e) return e;
    else {
        lli mid = (b + e) / 2;
        lli midCnt = count(m, mid);
        if( goal <= midCnt ) return search(m ,goal, b, mid);
        else return search(m, goal, mid, e);
    }

}

int main(){
    lli n, goal;
    cin >> n >> goal;

    vector<lli> m(n);

    for(auto i=0;i<n;i++) cin >> m[i];

    cout << search(m, goal, 0, ULLONG_MAX-1) << endl;

}

