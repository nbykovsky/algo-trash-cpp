// https://www.hackerrank.com/challenges/triple-sum/problem
#include <vector>
#include <iostream>
#include <set>
#include <algorithm>
#include <fstream>

using namespace std;

typedef  long long int lli;

lli search(vector<lli> &arr, lli v, lli b, lli e){
    if(v>arr[e-1]) return e-1;
    else if(v < arr[b]) return -1;
    if(b+1==e) return b;
    else {
        lli m = (b + e) / 2;
        if(v < arr[m]) return search(arr, v, b, m);
        else return search(arr, v, m, e);
    }

}

int main(){

    lli la, lb, lc;
    cin >> la >> lb >> lc;
    set<lli> a, c;
    set<lli> s;
    for(auto i=0;i<la;i++) {
        lli t;
        cin >> t;
        a.insert(t);
    }
    for(auto i=0;i<lb;i++) {
        lli t;
        cin >> t;
        s.insert(t);
    }
    for(auto i=0;i<lc;i++){
        lli t;
        cin >> t;
        c.insert(t);
    }

    vector<lli> aa(a.begin(), a.end());
    vector<lli> cc(c.begin(), c.end());

    sort(aa.begin(), aa.end());
    sort(cc.begin(), cc.end());

    lli counter = 0;
    for(const auto &bb: s){
        lli i1 = search(aa, bb, 0, aa.size())+1;
        lli i2 = search(cc, bb, 0, cc.size())+1;
        counter+=i1*i2;
    }
    cout << counter << endl;

}
