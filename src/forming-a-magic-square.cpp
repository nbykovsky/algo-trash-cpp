/*
 *solution of the problem https://www.hackerrank.com/challenges/magic-square-forming/problem
 */

#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

int main(){
    vector<int> s(9);
    for(auto i=0;i<9;i++) cin >> s[i];
    vector<vector<int>> squares = {
        {8,1,6,3,5,7,4,9,2},
        {6,1,8,7,5,3,2,9,4},
        {4,3,8,9,5,1,2,7,6},
        {2,7,6,9,5,1,4,3,8},
        {2,9,4,7,5,3,6,1,8},
        {4,9,2,3,5,7,8,1,6},
        {6,7,2,1,5,9,8,3,4},
        {8,3,4,1,5,9,6,7,2}
    };
    int min_cost = 1000;
    for(const auto &r: squares){
        int cost = 0;
        for(auto i=0;i<9;i++){
            cost+=abs(r[i] - s[i]);
        }
        if (cost<min_cost) min_cost = cost;
    }
    cout << min_cost << endl;


    return 0;
}
