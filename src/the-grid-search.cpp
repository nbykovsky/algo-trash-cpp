/*
 * problem https://www.hackerrank.com/challenges/the-grid-search/problem
 */
#include <iostream>
#include <vector>

using namespace std;

vector<size_t> find_all(string str, string pat){
    size_t start = -1;
    vector<size_t> ps;
    while((start=str.find(pat, start+1))!=-1) ps.push_back(start);
    return ps;
}

bool check(vector<string> grid, vector<string> pat, size_t r, size_t c){
    bool res = true;
    for(auto i=0;i<pat.size();i++){
        if(grid[i+r].find(pat[i],c)!=c) {
            res = false;
            break;
        }
    }
    return res;
}

int main(){
    int T;
    cin >> T;
    vector<bool> ans(T, false);
    for(auto i=0; i<T; i++){
        int R, C, r, c;
        cin >> R >> C;
        vector<string> grid(R);
        for(auto rr=0;rr<R;rr++) cin >> grid[rr];
        cin >> r >> c;
        vector<string> pattern(r);
        for(auto rr=0;rr<r;rr++) cin >> pattern[rr];

        for(auto j=0; j<=R-r;j++){
            vector<size_t> all = find_all(grid[j], pattern[0]);
            for(const auto &a: all){
                if(check(grid, pattern, j, a)){
                    j = R-r;
                    ans[i] = true;
                    break;
                }
            }
        }
    }

    for(const auto &a: ans)
        if(a) cout << "YES" << endl;
        else cout << "NO" << endl;
  
    return 0;
}

