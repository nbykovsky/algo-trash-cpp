// problem https://www.hackerrank.com/challenges/swap-nodes-algo/problem
#include <vector>
#include <iostream>
#include <utility>
#include <algorithm>

using namespace std;

bool search(vector<int> &arr, int v, int b, int e){
    if(b == e - 1){
        return arr[b] != v;
    } else {
        int m = (b + e) / 2;
        if(v<arr[m]) return search(arr, v, b, m);
        else return search(arr, v, m, e);
    }
}

vector<pair<int,int>> swap(vector<pair<int,int>> arr, int k, int d, int node){
    if(d%k == 0) arr[node] = make_pair(arr[node].second, arr[node].first);

    if(arr[node].first!=-1) arr = swap(arr, k, d + 1, arr[node].first);

    if(arr[node].second!=-1) arr = swap(arr, k, d + 1, arr[node].second);

    return arr;
}

void print(vector<pair<int,int>> &arr, int node){
    if(arr[node].first!=-1) print(arr, arr[node].first);
    cout << node + 1 << " ";
    if(arr[node].second!=-1) print(arr, arr[node].second);
}

int main(){
    unsigned int n, t;
    cin >> n;
    vector<pair<int,int>> tree(n);
    vector<int> arr1(n);
    vector<int> arr2(n);
    for(int i=0;i<n;i++){
        int a,b;
        cin >> a >> b;
        tree[i] = make_pair(max(a-1,-1),max(b-1,-1));
        arr1[i] = max(a-1,-1);
        arr2[i] = max(b-1,-1);
    }
    cin >> t;
    vector<int> queries(t);

    for(int i=0;i<t;i++) cin >> queries[i];

    sort(arr1.begin(), arr1.end());
    sort(arr2.begin(), arr2.end());

    int root;
    for(int i=0;i<n;i++){
        if(search(arr1, i, 0, n)&&search(arr2, i, 0, n)) {
            root = i;
            break;
        }
    }

    for(const auto &k: queries){
        tree = swap(tree, k, 1, root);
        print(tree, root);
        cout << endl;
    }


}