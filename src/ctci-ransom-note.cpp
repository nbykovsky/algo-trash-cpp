// problem https://www.hackerrank.com/challenges/ctci-ransom-note/problem
#include<map>
#include<iostream>
#include<vector>
#include <bits/stdc++.h>

using namespace std;

vector<string> split_string(string input_string) {
    string::iterator new_end = unique(input_string.begin(), input_string.end(), [] (const char &x, const char &y) {
        return x == y and x == ' ';
    });

    input_string.erase(new_end, input_string.end());

    while (input_string[input_string.length() - 1] == ' ') {
        input_string.pop_back();
    }

    vector<string> splits;
    char delimiter = ' ';

    size_t i = 0;
    size_t pos = input_string.find(delimiter);

    while (pos != string::npos) {
        splits.push_back(input_string.substr(i, pos - i));

        i = pos + 1;
        pos = input_string.find(delimiter, i);
    }

    splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));

    return splits;
}

int main(){
    int m, n;
    cin >> m >> n;
    string st1;
    string st2;

    cin.ignore();

    getline(cin, st1);
    getline(cin, st2);

    vector<string> magazine = split_string(st1);
    vector<string> note = split_string(st2);

    map<string, int> buf;

    for(auto const &w: magazine) {
        if(buf.count(w)==0) {
            buf[w] = 1;
        } else {
            buf[w]++;
        }
    }

    bool result = true;

    for(const string &w: note){
        if (buf.count(w)==0||buf[w]==0) {
            result = false;
            break;
        } else {
            buf[w]--;
        }
    }
    if (result) cout << "Yes" << endl;
    else cout << "No" << endl;

}
