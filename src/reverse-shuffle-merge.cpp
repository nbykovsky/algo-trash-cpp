// problem https://www.hackerrank.com/challenges/reverse-shuffle-merge/problem
#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

int main(){
    string s;
    getline(cin, s);

    map<char, int> all;
    map<char, int> req;

    for(const auto &c: s){
        if(all.count(c)) all[c]++;
        else all[c]=1;
    }

    int totalNumber=0;
    for(const auto &a: all){
        req[a.first]=a.second/2;
        totalNumber+=a.second/2;
    }

    reverse(s.begin(), s.end());

    int start = 0;
    int idx = 0;
    int minIndex;
    string str;

    while(totalNumber) {
        while(!req[s[idx]]) idx++;
        minIndex=idx;
        while (req[s[idx]] < all[s[idx]] && idx != s.length()) {

            if (s[idx] < s[minIndex] && req[s[idx]]>0) {
                minIndex = idx;
            }

            all[s[idx]]--;
            idx++;
        }

        if((req[s[idx]] == all[s[idx]])&&(s[idx] < s[minIndex])&&(req[s[idx]]>0)){
            minIndex = idx;
            all[s[idx]]--;
        } else if (minIndex != idx) {
            for (idx--; idx > minIndex; idx--) {
                all[s[idx]]++;
            }
        } else {
            all[s[idx]]--;
        }

        char c = s[minIndex];

        str += s[minIndex];
        req[s[minIndex]]--;
        totalNumber--;

        idx++;
    }

    cout << str << endl;
}
