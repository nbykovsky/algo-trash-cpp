/*
 * The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.
 * There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.
 * How many circular primes are there below one million?
 */

#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

typedef int ll;

vector<bool> eratospher(ll num){
    vector<bool> er(num+1, true);
    ll counter = 0;
    for(ll i=2; i<=num;i++){
        if(er[i]){
            counter++;
            ll a = i * 2;
            while(a <= num){
                er[a] = false;
                a+=i;
            };
        }
    }
    return er;
}

vector<ll> get(ll n) {
    vector<ll> output;
    int next = n;
    //log would be better solution, this also works
    int l = to_string(n).length();
    for(int i=0;i<l;i++){
        next = next / 10 + next % 10 * (ll)pow(10, l - 1);
        output.push_back(next);

    }
    return output;

}


int main(){
    int n=1000000;
    vector<bool> primes = eratospher(n);
    int  counter=0;
    for(ll i = 2; i <= n; i++) {
        if(primes[i]) {
            ll res = 1;
            for (const auto &a: get(i)) {
                if (!primes[a]) {
                    res = 0;
                    break;
                }
            }
            counter += res;
        }
    }
    cout << counter << endl;
}