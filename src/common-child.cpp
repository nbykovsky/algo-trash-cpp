//problem https://www.hackerrank.com/challenges/common-child/problem
#include <iostream>
#include <vector>

using namespace std;

int main(){
    string s1,s2;
    getline(cin, s1);
    getline(cin, s2);
    vector<vector<int>> dp(s2.length()+1, vector<int>(s1.length()+1,0));
    for(int i=1;i<=s2.length();i++){
        for(int j=1;j<=s1.length();j++){
            dp[i][j] = max(dp[i-1][j], dp[i][j-1]);
            if(s2[i-1]==s1[j-1]) dp[i][j]=max(dp[i-1][j-1]+1, dp[i][j]);

        }
    }
    cout << dp[s2.length()][s1.length()] << endl;
}