// problem https://www.hackerrank.com/challenges/pairs/problem
#include <iostream>
#include <set>
#include <vector>

using namespace std;

typedef  long long int lli;

int main(){
    lli n, k;
    cin >> n >> k;
    vector<lli> arr(n);
    set<lli> s;
    for(auto i=0;i<n;i++){
        lli t;
        cin >> t;
        arr[i] = t;
        s.insert(t);
    }

    lli counter = 0;
    for(const auto &a: arr){
        if (s.count(a+k)) counter++;
    }
    cout << counter << endl;

}