// problem https://www.hackerrank.com/challenges/abbr/problem
// todo: finish it
#include <iostream>
#include <vector>

using namespace std;

int main(){
    unsigned int q;
    cin >> q;
    cin.ignore();
    vector<bool> ans(q, true);
    for(auto i=0;i<q;i++){
        string a, b;
        getline(cin, a);
        getline(cin, b);

        int ia=0,ib=0;
        while(ib < b.length()){
            while(ia < a.length()){
                if(toupper(a[ia])==b[ib]) break;
                else if (!islower(a[ia])) {
                    ans[i] = false;
                    break;
                }
                if(ib==b.length()-1) ans[i] = false;

                ia++;
            }
            if(!ans[i]) break;
            ia++;
            ib++;
        }
        for(int j=ia;j<a.length();j++){
            if(isupper(a[j])){
                ans[i]=false;
                break;
            }
        }
    }
    for(const auto &a: ans){
        if(a) cout << "YES" << endl;
        else cout << "NO" << endl;
    }

}