/*
 * problem https://www.hackerrank.com/challenges/find-the-running-median/problem
 */

#include <iostream>
#include "../lib/median.h"

using namespace std;

int main(){
    int n;
    cin >> n;
    median<double> md([](double a, double b){return (a < b);});
    vector<double> ans(n);
    for(auto i=0;i<n;i++){
        double in;
        cin >> in;
        md.add(in);
        ans[i] = md.value();
    }
    cout.precision(1);
    for(const auto &a: ans) cout << fixed << a << endl;
}