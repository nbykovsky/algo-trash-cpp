// https://www.hackerrank.com/challenges/special-palindrome-again/problem
#include<iostream>
#include<vector>
#include<utility>
#include<fstream>

using namespace std;

int main(){
//    ifstream cin;
//    cin.open("/home/nikolai/Downloads/input02.txt");
    int n;
    string s;
    cin >> n;
    cin.ignore();
    getline(cin, s);

    int counter = 1;
    vector<pair<char, int>> buf;

    for(int i=1;i<s.length();i++){
        if(s[i-1]!=s[i]){
            buf.push_back(make_pair(s[i-1], counter));
            counter=1;
        }else counter++;
    }
    buf.push_back(make_pair(s[s.length()-1], counter));

    int counter1 = 0;
    for(int i=1;i<buf.size()-1;i++){
        if(buf[i-1].first==buf[i+1].first&&buf[i].second==1){
            counter1+=min(buf[i-1].second, buf[i+1].second);
        }
    }

    for(const auto &p: buf){
        counter1+=(p.second*(p.second+1))/2;
    }
    cout << counter1 << endl;
}