/*
 * problem https://www.hackerrank.com/challenges/fraudulent-activity-notifications/problem
 */
#include <iostream>
#include "../lib/median.h"

using namespace std;

int main(){
    int n,d;
    cin >> n >> d;
    int cnt = 0;
    median<double> md([](double a, double b){return (a < b);});
    vector<double> arr(n);
    for(auto i=0;i<n;i++){
        cin >> arr[i];
        if(i>=d&&arr[i]>=2*md.value()) cnt++;
        md.add(arr[i]);
        if(i>=d) md.del(arr[i-d]);
    }
    cout << cnt << endl;
}


