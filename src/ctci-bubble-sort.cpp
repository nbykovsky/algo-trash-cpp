// problem https://www.hackerrank.com/challenges/ctci-bubble-sort/problem
#include<iostream>
#include<vector>

using namespace std;

int main(){

    unsigned int n;
    cin >> n;
    vector<int> arr(n);
    for(int i=0;i<n;i++){
        cin >> arr[i];
    }

    int cnt=0;
    for(int i=0;i<n;i++){
        for(int j=0;j<n-1;j++){
            if(arr[j] > arr[j+1]){
                int tmp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = tmp;
                cnt++;
            }
        }
    }
    cout << "Array is sorted in " << cnt << " swaps." <<endl;
    cout << "First Element: " << arr[0] << endl;
    cout << "Last Element: " << arr[n-1] << endl;
}