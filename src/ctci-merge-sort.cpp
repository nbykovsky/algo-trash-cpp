// problem https://www.hackerrank.com/challenges/ctci-merge-sort/problem

#include <iostream>
#include <vector>
#include <utility>

using namespace std;


pair<long,vector<long>> merge(pair<long, vector<long>> &arr1, pair<long, vector<long>> &arr2){
    long cnt=0;
    vector<long> arr;
    long p1=0;
    long p2=0;
    while(p1 < arr1.second.size() || p2 < arr2.second.size()){
        if(p1 == arr1.second.size()){
            arr.push_back(arr2.second[p2]);
            p2++;
        } else if (p2 == arr2.second.size()){
            arr.push_back(arr1.second[p1]);
            p1++;
        } else if(arr1.second[p1] <= arr2.second[p2]){
            arr.push_back(arr1.second[p1]);
            p1++;
        } else {
            arr.push_back(arr2.second[p2]);
            cnt+=arr1.second.size() - p1;
            p2++;
        }
    }
    return make_pair(cnt + arr1.first + arr2.first, arr);
}

pair<long, vector<long>> mergeSort(vector<long> arr){
    if(arr.size() == 1){
        return make_pair(0, arr);
    } else if(arr.size() == 2){
        if(arr[0]>arr[1]){
            return make_pair(1, vector<long>({arr[1], arr[0]}));
        } else {
            return make_pair(0, arr);
        }
    }
    vector<long> arr1(&arr[0], &arr[arr.size()/2]);
    vector<long> arr2(&arr[arr.size()/2], &arr[arr.size()]);

    pair<long, vector<long>> pair1 = mergeSort(arr1);
    pair<long, vector<long>> pair2 = mergeSort(arr2);
    return merge(pair1, pair2);

}

int main(){

    unsigned int d;
    cin >> d;
    vector<long> output(d);
    for(int i=0;i<d;i++){
        unsigned int n;
        cin >> n;
        vector<long> arr(n);
        for(int j=0;j<n;j++) cin >> arr[j];

        output[i] = mergeSort(arr).first;
    }

    for(auto const &o: output){
        cout << o << endl;
    }
}