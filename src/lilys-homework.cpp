/*
 * problem https://www.hackerrank.com/challenges/lilys-homework/problem
 */

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

int process(vector<long int> src, vector<long int> trg, int n){
    map<long int, size_t> dict;
    for(auto i=0;i<n;i++) dict[src[i]]=i;
    int cnt=0;
    for(auto i=0;i<n;i++){
        if(src[i]!=trg[i]){
            cnt++;
            src[dict[trg[i]]]=src[i];
            dict[src[i]]=dict[trg[i]];
            dict[trg[i]]=i;
            src[i]=trg[i];
        }   
    }   
    return cnt;
}

int main(){

    int n;
    cin >> n;
    vector<long int> src(n);
    vector<long int> trg(n);
    map<long int, size_t> dict;
    for(auto i=0;i<n;i++) {
        long int tmp;
        cin >> tmp;
        src[i]=tmp;
        trg[i]=tmp;
    }
    sort(trg.begin(), trg.end(), greater<long int>());
    int option1 = process(src, trg, n);
    sort(trg.begin(), trg.end());
    int option2 = process(src, trg, n);
    cout << min(option1,option2) << endl;
   
    return 0;
}
