/*
 * problem https://www.hackerrank.com/challenges/countingsort4/problem
 */

#include <iostream>
#include <vector>

using namespace std;

int main(){

    int n;
    cin >> n;
    vector<vector<string>> arr(100, vector<string>());
    for(auto i=0;i<n;i++){
        size_t key;
        string value;
        cin >> key >> value;
        if(i+1<=n/2) value = "-";
        arr[key].push_back(value);
    }
    for(const auto &a: arr){
        for(const auto &b: a){
            cout << b << " "; 
        }
    }
    cout << endl;

    return 0;
}
