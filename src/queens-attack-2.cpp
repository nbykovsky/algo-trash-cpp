/*
 *
 * Problem https://www.hackerrank.com/challenges/queens-attack-2/problem
 *
 */

#include <iostream>
#include <vector>

using namespace std;

typedef pair<long,long> point;

bool is_between(point p1, point p2, point t){
    if(p1.first==p2.first&&t.first==p1.first)
        if((t.second-p1.second)*(t.second-p2.second)<=0) return true;
        else return false;
    if(p1.second==p2.second&&t.second==p1.second)
        if((t.first-p1.first)*(t.first-p2.first)<=0) return true;
        else return false;
    if(((p1.first - t.first)*(p1.second-p2.second)==(p1.first-p2.first)*(p1.second-t.second))&&((t.first-p1.first)*(t.first-p2.first)<=0)) return true;
    return false;
}

int main(){
    int n,k;
    point q;
    cin >> n >> k;
    cin >> q.first >> q.second;
    vector<point> ends={
            make_pair(0, q.second),
            make_pair(n+1, q.second),
            make_pair(q.first, 0),
            make_pair(q.first, n+1)
    };
    if(q.first >= q.second){
        ends.push_back(make_pair(n+1, q.second+n-q.first+1));
        ends.push_back(make_pair(q.first-q.second, 0));
    }else{
        
        ends.push_back(make_pair(q.first+n-q.second+1,n+1));
        ends.push_back(make_pair(0, q.second-q.first));
    }
    if(q.first+q.second > n){
        ends.push_back(make_pair(n+1,q.second-n+q.first-1));
        ends.push_back(make_pair(q.first-n+q.second-1, n+1));
    }else{
        ends.push_back(make_pair(0,q.first+q.second));
        ends.push_back(make_pair(q.first+q.second,0));
    }
    while(k--){
        int r,c;        
        cin >> r >> c;
        point tmp = make_pair(r,c);
        for(auto i=0;i<8;i++) if(is_between(q, ends[i], tmp)) ends[i]=tmp;
    }
    int cnt=0;
    for(const auto &e:ends) cnt+=max(abs(q.first-e.first)-1, abs(q.second-e.second)-1);
     
    cout << cnt << endl;

    return 0;
}


