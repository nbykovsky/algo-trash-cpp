/*
 * problem https://www.hackerrank.com/challenges/fibonacci-modified/problem
 */
#include <iostream>
#include "../lib/bigint.h"

using namespace std;

int main(){
    int t1, t2, n;
    cin >> t1 >> t2 >> n;
    vector<bigint> v;
    v.push_back(bigint(to_string(t1)));
    v.push_back(bigint(to_string(t2)));
    for(int i=2;i<n;i++) {
        bigint t = v[i-1];
        t.mult(v[i-1]);
        t.add(v[i-2]);
        v.push_back(t);
    }
    cout << v[n-1].to_str() << endl;

}