/*
 * problem https://www.hackerrank.com/challenges/absolute-permutation/problem
 */

#include <iostream>
#include <vector>
#include <set>

using namespace std;

int main(){
    int t;
    cin >> t;
    vector<vector<int>> p(t);
    for(auto i=0;i<t;i++){
        int n,k;
        cin >> n >> k;
        p[i]=vector<int>(n);
        if(n == k*2){
            for(auto j=1;j<=n;j++){
                int tmp = (j+k)%n;
                if (tmp == 0) p[i][j-1]=n;       
                else p[i][j-1]=tmp;
            }
        }else if(n < k*2){
            p[i]=vector<int>();
        }else{
            set<int> s;
            bool can=true;
            for(auto j=1;j<=k;j++) {
                    if((j+k==n-j-k+1) || s.count(j+k) || s.count(n-j-k+1)) {
                        p[i]=vector<int>();
                        can=false;
                        break;
                    }
                    s.insert(j+k);
                    s.insert(n-j+1-k);
                    p[i][j-1] = j+k;
                    p[i][n-j] = n-j+1-k;
            }
            
            for(auto j=k+1;j<=n-k&&can;j++){
                if(!s.count(j-k)){
                    p[i][j-1] = j-k;
                    s.insert(j-k); 
                }else if (!s.count(j+k)){
                    p[i][j-1] = j+k;
                    s.insert(j+k);               
                }else{
                    p[i]=vector<int>();
                    break;
                }
            }
        }
    }
    for(const auto &pp: p){
        if(pp.size()==0) cout << "-1" << endl;
        else {
                for(int i=0; i<pp.size();i++){
                    if(i == pp.size()-1) cout << pp[i] << endl;
                    else cout << pp[i] << " ";
                }
        }
    }

    return 0;
}
