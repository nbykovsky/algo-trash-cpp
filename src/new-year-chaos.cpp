// problem https://www.hackerrank.com/challenges/new-year-chaos/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays

#include <iostream>
#include <vector>

using namespace std;


class Processor {
    private:
        vector<int> storage;
        int pos;
    public:
        Processor(vector<int> storage){
            this->storage = storage;
            pos = 0;
        }
        unsigned long getOffset(){
            return storage.size() - pos;
        }
        int getNext(){
            int value = storage[pos];
            pos++;
            return pos - value;
        }
};

int main(){
    int n;
    cin >> n;
    for(int i=0;i<n;i++){
        unsigned int t;
        cin >> t;
        vector<int> q(t);
        for(int j=0;j<t;j++) cin >> q[j];

        bool good=true;

        int ind;
        int counter = 0;

        Processor p = Processor(q);
        while(p.getOffset()){
            ind = p.getNext();
            if (ind==0) continue;
            else if(ind==-1 && p.getOffset() >= 1){
                ind = p.getNext();
                if(ind==1){
                    // -1 1
                    counter++;
                    continue;
                }else if(ind==-1 && p.getOffset() >= 1){
                    ind = p.getNext();
                    if(ind==2) {
                        // -1 -1 2
                        counter+=2;
                        continue;
                    } else {
                        good=false;
                        break;
                    }
                }else if(ind==-2 && p.getOffset() >= 2){
                    ind = p.getNext();
                    if(ind==2 && p.getOffset() >= 2){
                        ind = p.getNext();
                        if(ind==-1 && p.getOffset() >= 1){
                            ind = p.getNext();
                            if(ind==2){
                                // -1 -2 2 -1 2
                                counter+=4;
                                continue;
                            } else {
                                good=false;
                                break;
                            }
                        } else if(ind==1){
                            //-1 -2 2 1
                            counter+=3;
                            continue;
                        }
                        else {
                            good=false;
                            break;
                        }
                    }
                }
            } else if(ind==-2&&p.getOffset() >=2){
                ind = p.getNext();
                if(ind==0&&p.getOffset() >=1){
                    ind = p.getNext();
                    if(ind==2){
                        // -2 0 2
                        counter+=3;
                        continue;
                    }
                }if(ind==1&&p.getOffset() >=1) {
                    ind = p.getNext();
                    if(ind==1){
                        // -2 1 1
                        counter+=2;
                        continue;
                    } else if (ind==-1&&p.getOffset() >=1){
                        ind = p.getNext();
                        if(ind==2){
                            // -2 1 -1 2
                            counter+=3;
                            continue;
                        } else {
                            good=false;
                            break;
                        }
                    } else if (ind==-2&&p.getOffset()>=2){
                        ind = p.getNext();
                        if(ind==2&&p.getOffset()>=1){
                            ind = p.getNext();
                            if(ind==1){
                                // -2 1 -2 2 1
                                counter+=4;
                                continue;
                            } if(ind==-1&&p.getOffset()>=1){
                                ind = p.getNext();
                                if(ind==2) {
                                    // -2 1 -2 2 -1 2
                                    counter+=5;
                                    continue;
                                } else {
                                    good=false;
                                    break;
                                }
                            } else {
                                good=false;
                                break;
                            }
                        } else {
                            good=false;
                            break;
                        }
                    } else {
                        good=false;
                        break;
                    }
                }
            } else {
                good=false;
                break;
            }
        }

        if (not good) cout << "Too chaotic" << endl;
        else cout << counter << endl;

    }

}