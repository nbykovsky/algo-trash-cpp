/*
 * Solution of problem https://www.hackerrank.com/challenges/organizing-containers-of-balls/problem
 */

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main(){

    int q;
    cin >> q;
    vector<bool> ans(q);
    for(auto qq=0;qq<q;qq++){
        int n;
        cin >> n;
        vector<long> r(n,0);
        vector<long> c(n,0);
        for(auto i=0;i<n;i++){
            for(auto j=0;j<n;j++){
                long tmp;
                cin >> tmp;
                r[i]+=tmp;
                c[j]+=tmp;
            }
        }
        sort(r.begin(), r.end());
        sort(c.begin(), c.end());
        bool b=true;
        for(auto i=0;i<n;i++){
            if(r[i]!=c[i]) {
                b=false;
                break;
            }
        }
        ans[qq]=b;
    }
    for(const auto &a: ans){
        if(a) cout << "Possible" << endl;
        else cout << "Impossible" << endl;
    }
    return 0;
}
