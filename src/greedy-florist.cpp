// problem https://www.hackerrank.com/challenges/greedy-florist/problem
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main(){
    unsigned int n, k;
    cin >> n >> k;

    vector<int> flowers(n);

    for(int i=0;i<n;i++) cin >> flowers[i];
    sort(flowers.begin(), flowers.end(), [](int a, int b){return a>b;});

    long cost=0;
    for(int i=0;i<n;i++){
        cost+=flowers[i]*(i/k + 1);
    }

    cout << cost << endl;

}