/*
 * problem https://www.hackerrank.com/challenges/unbounded-knapsack/problem
 */

#include <iostream>
#include <vector>

using namespace std;

int main(){
    int t;
    cin >> t;
    vector<int> ans(t);
    for(int i=0;i<t;i++){
        int n, k;
        cin >> n >> k;
        vector<int> arr(n);
        for(int j=0;j<n;j++) cin >> arr[j];
        vector<int> dp(k+1, 0);
        for(int j=1;j<k+1;j++){
            for(const auto &a: arr){
                if(a<=j){
                    dp[j] = max(dp[j], dp[j-a]+a);
                }
            }
        }
        ans[i] = dp[k];
    }
    for(const auto &a: ans) cout << a << endl;
}