/*
 * problem https://www.hackerrank.com/challenges/minimum-loss/problem
 */

#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <map>
#include <set>

using namespace std;

int main(){
    int n;
    cin >> n;
    vector<long long int> ps(n);
    map<long long int,set<int>> idx;
    for(auto i=0;i<n;i++) {
        cin >> ps[i];
        if(!idx.count(ps[i])) idx[ps[i]]=set<int>();
        idx[ps[i]].insert(i);
    }
    sort(ps.begin(), ps.end());
    long long int m = LLONG_MAX;
    for(auto i=1;i<n;i++){
        for(auto j=1;j<=i;j++){
            if((*idx[ps[i]].rbegin())<(*idx[ps[i-j]].begin())) {
                if(ps[i]-ps[i-j]<m&&ps[i]-ps[i-j]>0) m = ps[i]-ps[i-j];                    
                break;
            }
        }
    }
    cout << m << endl;
    return 0;
}
