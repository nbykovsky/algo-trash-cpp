// problem https://www.hackerrank.com/challenges/balanced-brackets/
#include <iostream>
#include <stack>
#include <vector>

using namespace std;

int main(){
    int n;
    cin >> n;
    vector<bool> ans(n, true);
    cin.ignore();
    for(auto i=0;i<n;i++){
        string s;
        getline(cin, s);
        stack<char> st;
        for(const auto &a: s){
            if(a == '{' | a == '[' | a == '(') {
                st.push(a);
            }else if (st.empty()){
                    ans[i] = false;
                    break;
            }else if((a == ']'&&st.top()=='[')||
                        (a == '}'&&st.top()=='{')||
                        (a == ')'&&st.top()=='(')) {
                    st.pop();
            }else {
                    ans[i] = false;
                    break;
            }
        }
        if(!st.empty()) ans[i] = false;
    }

    for(auto const &a: ans) {
        if (a) cout << "YES" << endl;
        else cout << "NO" << endl;
    }
}