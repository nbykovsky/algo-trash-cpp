// problem https://www.hackerrank.com/challenges/ctci-ice-cream-parlor/problem
#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>

using namespace std;

long search(vector<pair<int, int>> &arr, int v, unsigned long b, unsigned long e){
    if(b == e - 1) {
        if (v == arr[b].second) {
            return arr[b].first;
        } else {
            return -1;
        }
    } else {
        unsigned long m = (b + e) / 2;
        if(v < arr[m].second) {
            return search(arr, v, b, m);
        } else {
            return search(arr, v, m, e);
        }
    }

}


int main(){
    unsigned int t;
    cin >> t;
    vector<vector<long>> ans(t);
    for(int i=0;i<t;i++){
        unsigned int m, n;
        cin >> m >> n;
        vector<pair<int, int>> arr(n);
        for(int j=0;j<n;j++) {
            int q;
            cin >> q;
            arr[j] = make_pair(j+1, q);
        }

        sort(arr.begin(), arr.end(), [](pair<int, int> a, pair<int, int> b) {return a.second < b.second;});
        for(int p=0;p<n;p++){
            long idx = search(arr, m - arr[p].second, 0, arr.size());
            if(idx != -1){
                ans[i] = {arr[p].first, idx};
                sort(ans[i].begin(), ans[i].end());
                break;
            }
        }

    }
    for(const auto &p: ans) cout << p[0] << " " << p[1] << endl;

}