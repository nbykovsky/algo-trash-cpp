// problem https://www.hackerrank.com/challenges/sherlock-and-anagrams/problem
#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

int main(){
    unsigned int q;
    cin >> q;
    cin.ignore();
    vector<int> ans(q);
    for(int i=0;i<q;i++){
        string ss;
        getline(cin, ss);
        map<string, int> buf;


        for(int n=1;n<ss.length();n++){
            for(int m=0;m<ss.length()-n+1;m++){
                string s = ss.substr(m, n);
                sort(s.begin(), s.end());
                if(buf.count(s))
                    buf[s]++;
                else
                    buf[s] = 1;
            }
        }
        int counter = 0;
        for(const auto &b: buf){
            if(b.second>1) {
                counter += (b.second * (b.second - 1)) / 2;
            }
        }

        ans[i] = counter;
    }

    for(const auto &a: ans) cout << a << endl;

}
