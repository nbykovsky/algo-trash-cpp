/*
 * detect cycle in linked list
 * https://www.hackerrank.com/challenges/ctci-linked-list-cycle/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=linked-lists
 */
#include <iostream>

using namespace std;

struct Node {
    int data;
    struct Node * next;
    Node(int data, Node *node){
        this->next = node;
        this->data = data;
    }
};

bool has_cycle(Node* head){
    if(head == nullptr) return false;

    Node * slow = head;
    Node * fast = head;

    do {
        if (fast == nullptr) return false;
        fast=fast->next;
        if (fast == nullptr) return false;
        fast=fast->next;
        slow = slow->next;
    } while (slow != fast);

    return true;
}

int main(){
    Node * head = new Node(0, nullptr);
    Node * tmp = head;

    head = new Node(1, head);
    head = new Node(2, head);
    head = new Node(3, head);
    head = new Node(4, head);
    tmp->next = head;

    cout << has_cycle(head) << endl;
}