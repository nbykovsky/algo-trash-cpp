// problem https://www.hackerrank.com/challenges/making-candies/problem
// todo: finish it
#include <iostream>
#include <vector>
#include <utility>
#include <climits>
#include <cmath>

using namespace std;

typedef long long int lli;

lli myMin(lli a, lli b){
    if (a < b) return a;
    else return b;
}

lli myMax(lli a, lli b){
    if (a > b) return a;
    else return b;
}

lli MAX_RANGE = 2147483647;

int main(){
    lli m, w, p, n, x, y;
    cin >> m >> w >> p >> n;
    x = myMin(m, w);
    y = myMax(m, w);

    vector<pair<lli, lli>> buf((unsigned long long int)MAX_RANGE);

    lli count = 0;
    for(auto i=0;i<MAX_RANGE;i++){
        buf[i] = make_pair(count, x*y);
        count+=x*y;
        lli workers = count/p;
        count%=p;
        lli offset = myMax(workers-(y-x), 0);
        x+=myMin(y-x, workers);
        x+=offset/2;
        y+=offset - offset/2;
    }


//    cout << step << endl;
}