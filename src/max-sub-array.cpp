/*
 * https://www.hackerrank.com/challenges/maxsubarray/problem
 */

#include <iostream>
#include <climits>

using namespace std;

int main(){
    int t;
    cin >> t;
    for(int i=0;i<t;i++){
        int n;
        cin >> n;
        long int mx_end_here, mx_so_far, a, sum=0, mn;
        cin >> a;
        mx_end_here=a;
        mx_so_far=a;
        if(a<0)mn=a;
        else {
            mn=INT_MIN;
            sum=a;
        }
        for(int j=1;j<n;j++){
            cin >> a;
            mx_end_here = max(a, mx_end_here + a);
            mx_so_far = max(mx_so_far, mx_end_here);
            if(a>=0)sum+=a;
            else mn = max(mn, a);

        }
        if (sum) cout << mx_so_far << " " << sum << endl;
        else cout << mx_so_far << " " << mn << endl;
    }
}