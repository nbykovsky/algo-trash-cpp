// problem https://www.hackerrank.com/challenges/sherlock-and-valid-string/problem
#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

int main(){
    string s;
    getline(cin, s);

    map<char, int> m;
    map<int, int> f;
    vector<pair<int,int>> v;

    for(const auto &c: s){
        if(m.count(c)) m[c]++;
        else m[c]=1;
    }

    for(const auto &n: m){
        if(f.count(n.second)) f[n.second]++;
        else f[n.second]=1;
    }

    for(const auto &p: f) v.push_back(p);

    sort(v.begin(), v.end());

    if(v.size()<2) cout << "YES" << endl;
    else if(v.size()>2) cout << "NO" << endl;
    else if ((v[0].first+1==v[1].first&&v[1].second==1)||(v[0].first==1&&v[0].second==1)) cout << "YES" << endl;
    else cout << "NO" << endl;

}
