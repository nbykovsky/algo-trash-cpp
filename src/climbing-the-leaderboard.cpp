/*
 * solution of problem https://www.hackerrank.com/challenges/climbing-the-leaderboard/problem
 */

#include <iostream>
#include <map>
#include <vector>

using namespace std;

long b_search(vector<long> &vec, long l, long r, long subj){
    if(vec[l] <= subj || r == l+1) return l;
    if(vec[r-1] > subj) return r-1;
    long mid = (r + l)/2;
    if(subj <= vec[mid]) return b_search(vec, mid, r, subj);
    else return b_search(vec, l, mid, subj);
}

int main(){
    long n;
    cin >> n;
    map<long, long> mp;
    vector<long> v(n);
    long tmp;
    long rank=1;
    for(auto i=0; i<n;i++){
        cin >> tmp;
        v[i]=tmp;
        if(!mp.count(tmp)) mp[tmp]=rank++;
    }
    
    long m;
    cin >> m;
    vector<long> ans(m);
    long cur_ind;
    for(auto i=0;i<m;i++){
        cin >> tmp;
        cur_ind = b_search(v, 0, n, tmp);
        if(v[cur_ind]<=tmp) ans[i]=mp[v[cur_ind]];
        else ans[i]=mp[v[cur_ind]] + 1;
    }
    for(const auto &e: ans) cout << e << endl;

    return 0;
}


