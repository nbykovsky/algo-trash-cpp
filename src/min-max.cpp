// problem https://www.hackerrank.com/challenges/angry-children/problem
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main(){
    unsigned int n, k;
    cin >> n >> k;

    vector<long> arr(n);
    for(int i=0;i<n;i++) cin >> arr[i];

    sort(arr.begin(), arr.end());

    long unfair = INTMAX_MAX;
    for(int j=k-1;j<n;j++){
        unfair = min(unfair, arr[j] - arr[j-k + 1]);
    }

    cout << unfair << endl;
}