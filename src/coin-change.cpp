/*
 * problem https://www.hackerrank.com/challenges/coin-change/problem
 */
#include <iostream>
#include <vector>

using namespace std;

int main(){
    int n, m;
    cin >> n >> m;
    vector<int> cs(m);
    for(int i=0;i<m;i++) cin >> cs[i];
    vector<long long int> dp(n+1, 0);
    dp[0] = 1;
    for(int j=0;j<m;j++)
        for(int i=cs[j];i<=n;i++)
            dp[i]+=dp[i-cs[j]];
    cout << dp[n] << endl;
}