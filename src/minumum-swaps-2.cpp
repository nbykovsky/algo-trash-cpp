// problem https://www.hackerrank.com/challenges/minimum-swaps-2/problem

#include<vector>
#include<iostream>

using namespace std;

int main(){
    unsigned long n;
    cin >> n;
    vector<int> arr(n);
    for(auto i=0;i<n;i++){
        cin >> arr[i];
    }
    int counter = 0;
    for(auto i=0;i<n-1;i++){
        while(arr[i]!=i+1){
            int tmp = arr[i];
            arr[i] = arr[arr[i] - 1];
            arr[tmp - 1] = tmp;
            counter++;
        }
    }
    cout << counter << endl;

}