// https://www.hackerrank.com/challenges/ctci-making-anagrams/problem

#include<iostream>
#include<map>
#include<set>

using namespace std;

int main(){
    string as, bs;
    getline(cin, as);
    getline(cin, bs);
    map<char, int> ma, mb;
    set<char> all;

    for(const auto &a: as){
        if(ma.count(a)) {
            ma[a]++;
        } else {
            ma[a]=1;
        }
        if(!mb.count(a)) mb[a]=0;
        all.insert(a);
    }

    for(const auto &b: bs){
        if(mb.count(b)) {
            mb[b]++;
        } else {
            mb[b]=1;
        }
        if(!ma.count(b)) ma[b]=0;
        all.insert(b);
    }
    long counter=0;
    for(const auto &s: all){
        counter+=abs(ma[s]-mb[s]);
    }
    cout << counter << endl;
}