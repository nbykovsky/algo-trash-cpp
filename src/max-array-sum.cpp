// https://www.hackerrank.com/challenges/max-array-sum/problem
#include <iostream>
#include <vector>

using namespace std;

int main(){
    unsigned int n;
    cin >> n;
    vector<long int> arr(n);
    for(auto i=0;i<n;i++) cin >> arr[i];

    vector<long int> dp(n);
    dp[0] = arr[0];
    dp[1] = max(arr[0], arr[1]);
    long int ans = dp[1];
    for(auto i=2;i<n;i++){
        dp[i] = max(max(arr[i], dp[i-2]+arr[i]), ans);
        ans = max(ans, dp[i]);
    }

    cout << ans << endl;

}