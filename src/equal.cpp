/*
 * problem https://www.hackerrank.com/challenges/equal/problem
 */

#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

int main() {
    int t;
    cin >> t;
    vector<int> ans(t);
    for(int tt=0;tt<t;tt++){
        int n;
        cin >> n;
        vector <int> vs(n);
        for (int i=0;i<n;i++) cin >> vs[i];
        int mn=*min_element(vs.begin(),vs.end());
        int n1=0;
        for (int i=0;i<n;i++) {
            int tmp=floor((vs[i]-mn)/5.0);
            vs[i]-=(5*floor((vs[i]-mn)/5.0));
            n1+=tmp;
        }
        mn=*min_element(vs.begin(),vs.end());
        vector <int> rsd(5);
        for (int i=0;i<n;i++) rsd[vs[i]-mn]++;
        int n2=1*(rsd[1]+rsd[2])+2*(rsd[3]+rsd[4]);
        n2=min(n2,(rsd[0]+rsd[1]+rsd[4])+2*(rsd[2]+rsd[3]));
        n2=min(n2, (rsd[0]+rsd[3])+2*(rsd[1]+rsd[2]+rsd[4]));
        ans[tt] = n1+n2;
    }
    for(const auto &a: ans) cout << a << endl;
    return 0;
}