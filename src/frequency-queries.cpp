// https://www.hackerrank.com/challenges/frequency-queries/problem

#include<iostream>
#include<map>
#include<vector>

using namespace std;

int main(){
    long n;

    cin >> n;

    map<long, long> freq;
    map<long, long> der;
    der[0]=0;
    vector<long> ans;

    for(auto i=0;i<n;i++){
        int t, q;
        cin >> t >> q;
        long prev, cur;

        switch(t) {
            case 1:

                if(freq.count(q)){
                    prev = freq[q];
                    freq[q]++;
                    cur = freq[q];
                } else {
                    prev = 0;
                    freq[q]=1;
                    cur = 1;
                }
                if(der.count(prev) && der[prev] > 0) der[prev]--;

                if(der.count(cur)) der[cur]++;
                else der[cur]=1;

                break;
            case 2:
                if(freq.count(q) && freq[q]>0){
                    prev = freq[q];
                    freq[q]--;
                    cur = freq[q];

                    if(der.count(prev) && der[prev] > 0) der[prev]--;

                    if(der.count(cur)) der[cur]++;
                    else der[cur]=1;
                }
                break;
            case 3:
                if (der.count(q) && der[q]>0) ans.push_back(1);
                else ans.push_back(0);
                break;
            default:
                break;
        }

    }

    string str;
    for(auto const &a: ans){
        str+=to_string(a)+'\n';
    }
    cout << str << endl;

}