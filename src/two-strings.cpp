// problem
#include <iostream>
#include <set>
#include <vector>

using namespace std;

int main(){
    unsigned int p;
    cin >> p;

    cin.ignore();

    vector<string> ans(p);
    for(int i=0;i<p;i++){
        string s1;
        string s2;
        getline(cin, s1);
        getline(cin, s2);

        set<char> buf;
        for(const char &c: s1){
            buf.insert(c);
        }

        bool shared = false;
        for(const char &c: s2){
            if(buf.count(c)>0){
                shared = true;
                break;
            }
        }

        if (shared) {
            ans[i] = "YES";
        } else {
            ans[i] = "NO";
        }

    }

    for(const auto &a: ans) cout << a << endl;

}