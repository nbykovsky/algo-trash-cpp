/*
 * problem https://www.hackerrank.com/challenges/sherlock-and-cost/problem
 */

#include <iostream>
#include <vector>

using namespace std;

int main(){
    int t;
    cin >> t;
    vector<int> ans(t);
    for(int j=0;j<t;j++){
        int n;
        cin >> n;
        vector<int> l(n);
        vector<int> h(n);
        vector<int> b(n);
        cin >> b[0];
        l[0] = 0;
        h[0] = 0;
        for(int i=1;i<n;i++){
            cin >> b[i];
            l[i] = max(l[i-1], h[i-1] + abs(b[i-1] - 1));
            h[i] = max(h[i-1] + abs(b[i] - b[i-1]), l[i-1] + abs(b[i] - 1));
        }
        ans[j] = max(l[n-1], h[n-1]);
    }
    for(const auto &a: ans) cout << a << endl;

}