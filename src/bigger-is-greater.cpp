/*
 * problem https://www.hackerrank.com/challenges/bigger-is-greater/problem
 */

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void process(vector<char> &str){
    int point=-1;
    for(auto i=str.size()-2;i>=0;i--){
        if (str[i]<str[i+1]) {
                point=i;
                break;
        }
    }
    if (point==-1){
        string s = "no answer";
        vector<char> t(s.begin(), s.end());
        str=t;
        return;
    }
    int ref=point+1;
    for(auto j=point+2;j<str.size();j++){
        if(str[j]<str[ref]&&str[point]<str[j]) ref=j;
    }
    int tmp = str[point];
    str[point] = str[ref];
    str[ref] = tmp;
    sort(str.begin()+point+1, str.end());
    return;
}


int main(){
    int t;
    cin >> t;
    vector<string> ans(t);
    for(auto i=0;i<t;i++){
        string str;
        cin >> str;
        vector<char> v(str.begin(), str.end());
        process(v);
        string output(v.begin(), v.end());
        ans[i]=output;
    }
    for(const auto &a: ans) cout << a << endl;
    
    return 0;     
}
