/*
 * problem https://www.hackerrank.com/challenges/sam-and-substrings/problem
 */

#include <iostream>
#include <algorithm>
#include <vector>
#include <cmath>

using namespace std;

long long int go(vector<int> ints){
    vector<long long int> dp(ints.size());
    dp[0] = ints[0];
    long long int sum=dp[0];
    long long int mod = (long long int)pow(10, 9)+7;
    for(int i=1;i<ints.size();i++){
        dp[i] = (dp[i-1]*10+(i+1)*ints[i])%mod;
        sum=(dp[i]+sum)%mod;
    }
    return sum;
}

int main(){
    string str;
    getline(cin, str);
    vector<int> ints(str.size());
    transform(str.begin(), str.end(), ints.begin(), [](char a) {return a - '0';});
    cout << go(ints) << endl;
}